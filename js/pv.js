(function($, Drupal) {
    Drupal.behaviors.pv_ajax = {
    attach: function (context, settings) {
      const PV_ONCE = once('pv_ajax', 'body', context);
      const tstamp = new Date();
      $(PV_ONCE).each(() => {
        $.ajax({
          url: settings.pv.url + `&t=${tstamp.getTime()}`
        }).done((count) => {
          $(`#${settings.pv.element_id}`).html(count.toLocaleString());
        });
      })
    }
  }
}(jQuery, Drupal));
