<?php

namespace Drupal\page_visits\Plugin\Block;

use Drupal\Core\Url;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder\Form\AddBlockForm;
use Drupal\layout_builder\Form\UpdateBlockForm;
use Drupal\block\BlockForm;
use Drupal\page_visits\PageVisitsManager;
/**
 * Provides an example block.
 *
 * @Block(
 *   id = "page_visits_block",
 *   admin_label = @Translation("PV Block"),
 *   category = @Translation("Page Visits")
 * )
 */
class PageVisitBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Max time'),
      '#description' => $this->t('Time in seconds for cookies to expire'),
      '#default_value' => \Drupal::service('config.factory')->getEditable('page_visits.settings')->get('time'),
    ];

    $form['class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Class'),
      '#description' => $this->t('Class attribute'),
      '#default_value' => \Drupal::service('config.factory')->getEditable('page_visits.settings')->get('class'),
    ];

    $form['prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix'),
      '#description' => $this->t('Prefix element'),
      '#default_value' => \Drupal::service('config.factory')->getEditable('page_visits.settings')->get('prefix'),
    ];

    $form['suffix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Suffix'),
      '#description' => $this->t('Suffix element'),
      '#default_value' => \Drupal::service('config.factory')->getEditable('page_visits.settings')->get('suffix'),
    ];

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    \Drupal::service('config.factory')->getEditable('page_visits.settings')
      ->set('time', $form_state->getValue('time'))
      ->set('class', $form_state->getValue('class'))
      ->set('prefix', $form_state->getValue('prefix'))
      ->set('suffix', $form_state->getValue('suffix'))
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $route_id = \Drupal::service('page_visits.cookie_service')->getRouteID();
    $url = Url::fromRoute('page_visits.api', ['route_id'=> $route_id])->toString();
    $class = \Drupal::service('config.factory')->getEditable('page_visits.settings')->get('class');
    $prefix = \Drupal::service('config.factory')->getEditable('page_visits.settings')->get('prefix');
    $suffix = \Drupal::service('config.factory')->getEditable('page_visits.settings')->get('suffix');
    $build['content'] = [
      '#markup' => $prefix . '<p id="' . PageVisitsManager::ELEMENT_ID . '" class="' . $class . '" ></p>' . $suffix,
      '#cache' => [
        'contexts' => [
          'url.path'
        ],
      ],
      '#attached' => [
        'library' => [
          'core/once',
          'core/jquery',
          'core/drupal',
          'core/drupalSettings',
          'page_visits/pv'
        ],
        'drupalSettings' => [
          'pv' => [
            'time' => time(),
            'url' => $url,
            'route_id' => $route_id,
            'element_id' => PageVisitsManager::ELEMENT_ID,
          ],
        ]
      ]
    ];
    return $build;
  }
}