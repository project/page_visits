<?php

namespace Drupal\page_visits;

class PageVisitsManager {

  const ELEMENT_ID = 'pv_count';

  const COOKIE_PREFIX = 'pv_';

  const QUERY_KEY = 'route_id';

  const DEFAULT_MAX_AGE = 60;
}