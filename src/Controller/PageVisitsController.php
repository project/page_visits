<?php

namespace Drupal\page_visits\Controller;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\page_visits\PageVisitsManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Returns responses for page visits routes.
 */
class PageVisitsController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $request) {
    $this->request = $request->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * Builds the response.
   */
  public function fetch() {
    $cookies  = $this->request->cookies->all();
    $query = $this->request->query->all();
    $route_id = !empty($query[PageVisitsManager::QUERY_KEY]) ? $query[PageVisitsManager::QUERY_KEY] : FALSE;
    if (empty($route_id)) {
      return $this->getCachedResponse(PageVisitsManager::COOKIE_PREFIX, time(), 0);
    }
    $result = \Drupal::service('page_visits.cookie_service')->procesPVCCookie($cookies, $route_id);
    return $this->getCachedResponse($route_id, $result['timestamp'], $result['count']);
  }

  /**
   * Cached API Requests / Expires every x minutes.
   */
  protected function getCachedResponse($route_id, $time, $view_count) {
    $metadata = new CacheableMetadata();
    $metadata->setCacheContexts(['url.query_args']);
    $response = new CacheableJsonResponse($view_count);
    $response->addCacheableDependency($metadata);
    $response->setPublic();
    $response->setMaxAge(5);
    $response->headers->setCookie(new Cookie($route_id, $time));
    return $response;
  }
}
