<?php

namespace Drupal\page_visits\Service;

use Drupal\Core\State\State;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\page_visits\PageVisitsManager;
use Drupal\Core\Routing\CurrentRouteMatch;
class PVCookieService {

  /**
   * {@inheritdoc}
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  protected $routeMatch;
  /**
   * Constructs PVCookieService.
   *
   * @param Symfony\Component\HttpFoundation\RequestStack;
   *  The request.
   *
   * @param Drupal\Core\State\State;
   *. The state.
   *
   */
  public function __construct(RequestStack $request, CurrentRouteMatch $routeMatch, State $state){
    $this->request = $request;
    $this->routeMatch = $routeMatch;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function procesPVCookie($cookies = [], $route_id = '') {
    $validCookies = $this->detectValidCookies($cookies);
    $timestamp = !empty($validCookies[$route_id]) ? $validCookies[$route_id] : 0;
    return $this->processCount($route_id, $timestamp);
  }

  /**
   * {@inheritdoc}
   */
  public function getRouteID() {
    $rawParams = $this->routeMatch->getRawParameters()->all();
    return $this->buildKey($rawParams);
  }

  /**
   * {@inheritdoc}
   */
  protected function detectValidCookies($cookies) {
    $prefix = PageVisitsManager::COOKIE_PREFIX;
    $valid_cookies = [];
    foreach ($cookies as $key => $value) {
      if (str_contains($key, $prefix)) {
        $valid_cookies[$key] = $value;
      }
    }
    return $valid_cookies;
  }

  /**
   * {@inheritdoc}
   */
  protected function getMaxAge() {
    $time = \Drupal::service('config.factory')->getEditable('page_visits.settings')->get('time');
    if (empty($time)) {
      return PageVisitsManager::DEFAULT_MAX_AGE . time();
    }
    return $time;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildKey($rawParams) {
    if (empty($rawParams)) {
      return FALSE;
    }
    $id = PageVisitsManager::COOKIE_PREFIX;
    foreach ($rawParams as $key => $rawParam) {
      $id .= '_' . $key . '_' . $rawParam;
    }
    return $id;
  }

  /**
   * {@inheritdoc}
   */
  protected function processCount($route_id, $timestamp) {
    $count = 1;
    if ($this->state->get($route_id)) {
      if ((time() - $timestamp) > $this->getMaxAge() || empty($timestamp)) {
        $count = intval($this->state->get($route_id)) + 1;
        $this->state->set($route_id, $count);
        $timestamp = time();
      }
      $count = $this->state->get($route_id);
    } else {
      $timestamp = time();
      $this->state->set($route_id, $count);
    }
    return [
      'count' => $count,
      'timestamp' => $timestamp,
    ];
  }
}
